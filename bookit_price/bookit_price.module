<?php

/**
 * Implements hook_bookit_calendar_not_bookable_query_alter().
 */
function bookit_price_bookit_calendar_not_bookable_query_alter(&$query, &$conditions, $context) {
  $query->leftjoin('bookit_price', 'p', 'p.item_id = i.item_id AND p.date = c.date');
  $conditions->condition('p.amount', 0);
  $conditions->isNull('p.amount');
}

/**
 * Implements hook_views_api().
 */
function bookit_price_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'bookit_price') . '/includes/views',
  );
}

/**
 * Implements hook_entity_delete().
 */
function bookit_price_entity_delete($entity, $entity_type) {
  if ($entity_type == 'bookit_item') {
    list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

    // If bookable item deleted then delete the pricing rows.
    db_delete('bookit_price')
      ->condition('item_id', $id)
      ->execute();
  }
}

/**
 * Implements hook_form_alter().
 */
function bookit_price_form_bookit_calendar_admin_form_alter(&$form, &$form_state) {
  $bookit_item = $form_state['bookit_item'];

  // Get default currency code.
  $currency_code = commerce_default_currency();

  foreach (element_children($form['calendar']) as $day) {
    // Price amount input.
    $form['calendar'][$day]['bookit_price']['#element_validate'][] = 'commerce_price_field_widget_validate';
    $form['calendar'][$day]['bookit_price']['#title'] = t('Price (@currency_code)', array('@currency_code' => $currency_code));

    $form['calendar'][$day]['bookit_price']['amount'] = array(
      '#type' => 'textfield',
      '#date' => $form['calendar'][$day]['#date'],
      '#default_value' => '',
      '#size' => 4,
    );

    $form['calendar'][$day]['bookit_price']['currency_code'] = array(
      '#type' => 'value',
      '#value' => $currency_code,
    );

    // Get default price for a date.
    $price = db_select('bookit_price', 'p')
      ->fields('p', array('amount', 'currency_code'))
      ->condition('p.item_id', $bookit_item->item_id)
      ->condition('p.date', $form['calendar'][$day]['#date'])
      ->execute()
      ->fetchAssoc();

    if (!empty($price['amount'])) {
      $currency = commerce_currency_load($price['currency_code']);

      // Convert the price amount to a user friendly decimal value.
      $default_amount = commerce_currency_amount_to_decimal($price['amount'], $currency['code']);

      // Run it through number_format() to ensure it has the proper number of
      // decimal places.
      $default_amount = number_format($default_amount, $currency['decimals'], '.', '');

      $form['calendar'][$day]['bookit_price']['amount']['#default_value'] = $default_amount;
      $form['calendar'][$day]['bookit_price']['currency_code']['#value'] = $price['currency_code'];
    }
  }

  $form['#submit'][] = 'bookit_price_bookit_calendar_admin_form_submit';
}

/**
 * Submit callback for bookit_calendar_admin_form.
 */
function bookit_price_bookit_calendar_admin_form_submit($form, &$form_state) {
  $bookit_item = $form_state['bookit_item'];
  $currency_code = commerce_default_currency();

  foreach ($form_state['values']['calendar'] as $day => $value) {
    // Save the price.
    db_merge('bookit_price')
    ->key(array(
      'item_id' => $bookit_item->item_id,
      'date' => $form['calendar'][$day]['#date'],
    ))
    ->fields(array(
      'item_id' => $bookit_item->item_id,
      'date' => $form['calendar'][$day]['#date'],
      'amount' => (int) $value['bookit_price']['amount'],
      'currency_code' => isset($value['bookit_price']['currency_code']) ? $value['bookit_price']['currency_code'] : $currency_code,
      'data' => serialize(array()),
    ))
    ->execute();
  }
}
